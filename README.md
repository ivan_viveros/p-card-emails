[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

<img src="https://i.ibb.co/fYdTP36/p-card-new.png" alt="P-Card logo" width="86px" />

# P-Card Email Templates

This projects contains all the transactional email templates for P-Card.

## Developing new templates

To start developing, just create a new template inside `src/emails` and make sure you have:

- [The MJML CLI](https://www.npmjs.com/package/mjml-cli)
- [The adequate MJML extension installed for your code editor](https://documentation.mjml.io/#tooling)

## Helpful NPM Scripts

This project contains some helpful yarn scripts to make your development workflow even better.

To **transpile a mjml template to html** run the command `yarn build` and add the **template name** you want to build.

```bash
yarn build Email-02.0-TransferMade
```

To **send a test email** run the command `yarn send` and add the **template name** you want to send.

Bear in mind that you will need your `.env` credentials.

```bash
yarn send Email-02.0-TransferMade
```

Happy coding! ✨
