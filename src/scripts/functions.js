const { default: axios } = require("axios");
const { exec } = require("child_process");
const fs = require("fs");
const mustache = require("mustache");
const path = require("path");

exports.removeTemplateExtension = (templateName) => {
  if (templateName.endsWith(".html") || templateName.endsWith(".mjml")) {
    templateName = templateName.slice(0, -5);
  }

  return templateName;
};

const ensureDirExists = (dir) => {
  if (!fs.existsSync(dir)) fs.mkdirSync(dir);
};

exports.transpileMjml = (
  templateName,
  onSuccess = () => console.log("🏗  MJML succesfully transpiled")
) => {
  ensureDirExists(path.resolve("build"));

  const mjmlBuildCommand = `mjml src/emails/${templateName}.mjml -o build/${templateName}.html`;

  exec(mjmlBuildCommand, (error) => {
    if (error) throw new Error(error);
    onSuccess();
  });
};

exports.injectTemplateData = (htmlTemplateName, templateData) => {
  const htmlTemplate = fs
    .readFileSync(path.resolve("build", htmlTemplateName + ".html"))
    .toString();

  const renderedHTML = mustache.render(htmlTemplate, templateData);

  console.log("💉  Variables injected into the template");
  return renderedHTML;
};

exports.sendEmail = async (emailData) => {
  const api = axios.create({
    baseURL: "https://api.postmarkapp.com",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-Postmark-Server-Token": process.env.POSTMARK_SERVER_TOKEN,
    },
  });

  try {
    const response = await api.post("/email", emailData);
    console.log("📤  Email sent successfully!", response.data);
  } catch (error) {
    console.error(error);
  }
};
