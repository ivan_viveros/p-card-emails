const { transpileMjml, removeTemplateExtension } = require("./functions");

let templateName = process.argv[2];

if (!templateName) throw new Error(`Missing "template name" in the CLI`);

templateName = removeTemplateExtension(templateName);

transpileMjml(templateName);
