require("dotenv").config({});

const {
  removeTemplateExtension,
  transpileMjml,
  injectTemplateData,
  sendEmail,
} = require("./functions");

let templateName = process.argv[2];

if (!templateName) throw new Error(`Missing "template name" in the CLI`);

templateName = removeTemplateExtension(templateName);

const templateVariables = {
  ctaLink: "https://app.p-card.com/",
  to: process.env.TO,
  cancelSubscriptionLink: "https://sendgrid.com/",
  name: "John Doe",
};

transpileMjml(templateName, () => {
  sendEmail({
    To: process.env.TO,
    From: process.env.FROM,
    Subject: templateName,
    HtmlBody: injectTemplateData(templateName, templateVariables),
  });
});
